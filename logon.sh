#!/usr/bin/env bash
google-oauthlib-tool --credentials credentials.json --client-secrets client_secrets.json --scope "https://www.googleapis.com/auth/spreadsheets"
