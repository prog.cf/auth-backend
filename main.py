import datetime
import json
import os
import requests
from selenium import webdriver
import time


HANDLE = "prog.cf"
PASSWORD = os.environ["CODEFORCES_PASSWORD"]

SCOPES = ["https://www.googleapis.com/auth/spreadsheets"]
SPREADSHEET_ID = "1MwUUg5TeirvojAUzoAJiM6gp9-lNKg3H6CntMHDGmT8"

CLIENT_ID = "553186379502-rgi765t2ldnhg44b4l240f374kvuuobb.apps.googleusercontent.com"
CLIENT_SECRET = os.environ["GOOGLE_CLIENT_SECRET"]
REFRESH_TOKEN = os.environ["GOOGLE_REFRESH_TOKEN"]


auth = requests.post("https://oauth2.googleapis.com/token", data={
    "client_id": CLIENT_ID,
    "client_secret": CLIENT_SECRET,
    "refresh_token": REFRESH_TOKEN,
    "grant_type": "refresh_token"
}).json()

access_token = auth["access_token"]


options = webdriver.ChromeOptions()
options.add_argument("--no-sandbox")
options.add_argument("--disable-gpu")
options.add_argument("--disable-dev-shm-usage")
options.headless = True

driver = webdriver.Chrome(options=options)

driver.get("https://codeforces.com/enter?lang=en")
driver.find_element_by_xpath("//input[@name='handleOrEmail']").send_keys(HANDLE)
driver.find_element_by_xpath("//input[@name='password']").send_keys(PASSWORD)
driver.find_element_by_id("remember").click()
driver.find_element_by_xpath("//input[@value='Login']").click()

time.sleep(5)
driver.get("https://codeforces.com/")
time.sleep(5)
driver.get("https://codeforces.com/")
time.sleep(5)

cookies = json.dumps(driver.get_cookies())

requests.post(
    f"https://sheets.googleapis.com/v4/spreadsheets/{SPREADSHEET_ID}/values:batchUpdate",
    headers={"Authorization": f"Bearer {access_token}"},
    json={
        "valueInputOption": "RAW",
        "data": {
            "range": "A2:B2",
            "majorDimension": "ROWS",
            "values": [[datetime.datetime.utcnow().isoformat(), cookies]]
        }
    }
)
